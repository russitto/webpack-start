var ExtractTextPlugin = require("extract-text-webpack-plugin"),
  HtmlWebpackPlugin = require('html-webpack-plugin'),
  extractScss = new ExtractTextPlugin('[name].css');

module.exports = {
  context: __dirname + "/src",
  entry: {
    index: "./index.es6"
  },
  output: {
    path: __dirname + "/dist",
    filename: "[name].js",
    chunkFilename: "[id].js"
  },
  module: {
    loaders: [
      {
        test: /\.es6\.js?$/i,
        loader: 'babel',
        query: {
          presets: ['es2015']
        }
      },
      {
        test: /\.scss$/i,
        loader: extractScss.extract("style", "css!sass")
      },
      {
        test: /\.j2$/i,
        loader: "template-html-loader?engine=nunjucks&title=Hola"
      }
    ]
  },
  plugins: [
    extractScss,
    new HtmlWebpackPlugin({
      filename: 'index.html',
      template: './index.j2'
    }),
    new HtmlWebpackPlugin({
      filename: 'about.html',
      template: './about.j2'
    })
  ]
};
